# Algebraic data types

* Enumeration types
* Beyond enumerations
* Algebraic data types in general
* Pattern-matching
* Case expressions
* Recursive data types
